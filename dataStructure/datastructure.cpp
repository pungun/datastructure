#include <iostream>
#include<string>
#include<cctype>
using std::string;
//연결리스트를 이용한 스택 구현
template<class T> 
class stack {
private:
	//데이터를 저장하기 위한 연결리스트
	class dataList {
	public:
		T value;		
		dataList* next;	
		//기본생성자
		dataList() {
			this->value = 0;
			this->next = NULL;
		}
		//데이터만 받는 생성자
		dataList(T data) {
			this->value = data;
			this->next = NULL;
		}
		//데이터와 다음 리스트를 받는 생성자
		dataList(T data,dataList* next) {
			this->value = data;
			this->next = next;
		}
	};
	dataList* list;
	int length;

public:
	//기본생성자
	stack() {
		this->length = 0;
		this->list = NULL;
	}
	//마지막에 삽입된 데이터 출력
	T pop() {
		//스택이 비어있다면 에러 출력후 0 리턴
		if (list == NULL) {
			std::cout << "ERROR:스택이 비어있습니다.\n";
			return 0;
		}
		//데이터의 값을 저장 후 다음 데이터로 이동, 이전 데이터 삭제
		T output=list->value;
		dataList* tmp = list;
		list = list->next;
		delete tmp;
		this->length -= 1;
		return output;
	}
	//리스트의 맨 앞에 데이터 삽입 후 결과 반환
	bool push(T data) {
		dataList* tmp = list;
		//리스트의 맨 앞에 데이터 삽입 시도
		try {
			list = new dataList(data,list);
		}
		//할당이 실패한 경우 에러메세지 출력 후 변경 취소,false 리턴
		catch (std::bad_alloc) {
			std::cout << "ERROR:할당이 실패했습니다.\n";
			list = tmp;
			return false;
		}
		//할당이 성공했으면 길이 증가 후 true 리턴
		this->length += 1;
		return true;
	}
	//길이 반환
	int getLength() {
		return this->length;
	}
	//스택이 제거될 때 데이터 모두 삭제
	~stack() {
		dataList* now = this->list;
		dataList* next;
		//데이터가 없을 때 까지 반복
		while (now != NULL) {
			next = now->next;
			delete now;
			now = next;
		}
	}
};
//큐 구현
class queue {
private:
	//데이터를 저장하기 위한 이너클래스
	class dataList {
	public:
		int data;
		dataList* next;
		dataList* prev;
		//기본생성자
		dataList() {
			this->data = 0;
			this->next = NULL;
			this->prev = NULL;
		}
		//데이터만 받는 생성자
		dataList(int n) {
			this->data = n;
			this->next = NULL;
			this->prev = NULL;
		}
		//데이터와 다음 리스트를 받는 생성자
		dataList(int n, dataList* next) {
			this->data = n;
			this->next = next;
			this->prev = NULL;
		}
	};
	dataList* first;
	dataList* last;

	int length;

public:
	//기본생성자
	queue() {
		this->length = 0;
		this->first = NULL;
		this->last = NULL;
	}
	//리스트의 마지막 데이터 출력
	int dequeue() {
		if (last == NULL) {
			std::cout << "큐가 비어있습니다.";
			return 0;
		}
		int output = last->data;
		dataList* tmp = last;
		last = last->prev;
		delete tmp;
		this->length -= 1;
		if (this->length == 0)
			this->first = NULL;
		return output;
	}
	//리스트의 맨 앞에 데이터 삽입 후 결과 반환
	bool enqueue(int data) {
		dataList* tmp = first;
		try {
			first = new dataList(data, first);
			if (first->next != NULL)
				first->next->prev = first;
			if (last == NULL)
				last = first;
		}
		catch (std::bad_alloc) {
			std::cout << "할당이 실패했습니다.\n";
			first= tmp;
			return false;
		}
		this->length += 1;
		return true;
	}
	//길이 반환
	int getLength() {
		return this->length;
	}
	//소멸자를 호출하면 데이터 모두 반환
	~queue() {
		dataList* now = this->first;
		dataList* next = now->next;
		while (now != NULL) {
			next = now->next;
			delete now;
			now = next;
		}
	}
};
//이진 트리 구현
class BST {
private:
	//데이터가 저장되는 이너클래스
	class node {
	public:
		string value;
		node* left;
		node* right;
		//기본 생성자
		node() {
			value = "";
			left = NULL;
			right = NULL;
		}
		//값만 받는 생성자
		node(string data) {
			value = data;
			left = NULL;
			right = NULL;
		}
	};
	node* root;
public:
	//기본 생성자
	BST() {
		root = NULL;

	}
	//데이터를 트리에 추가한다.
	void add(string data) {
		//root가 NULL이면 데이터를 추가하고 root로 한다.
		if (root == NULL) {
			root = new node(data);
			return;
		}
		else {
			node* now = root;
			while (true) {
				switch (data.compare(now->value)) {
				//추가할 데이터가 앞에 있을 때
				case -1:
					//현재 노드의 왼쪽이 비어있다면 왼쪽에 추가 후 종료
					if (now->left == NULL) {
						now->left = new node(data);
						return;
					}
					//아니면 현재 노드를 왼쪽으로 옮긴 후 계속
					else {
						now = now->left;
						continue;
					}
				//중복일 결우 종료
				case 0:return;
					//추가할 데이터가 뒤에 있을 때
				case 1:
					//현재 노드의 오른쪽이 비어있다면 오른쪽에 추가 후 종료
					if (now->right == NULL) {
						now->right = new node(data);
						return;
					}
					//아니면 현재 노드를 오른쪽으로 옮긴 후 계속
					else {
						now = now->right;
						continue;
					}
				}
			}
		}
	}
	//데이터가 존재하는지 검색한다.
	bool search(string data) {
		//root가 NULL이면 종료
		if (root == NULL)
			return false;
		else {
			node* now = root;
			while (true) {
				switch (data.compare(now->value)) {
					//찾을 데이터가 앞에 있을 때
				case -1:
					//현재 노드의 왼쪽이 비어있다면 종료
					if (now->left == NULL)
						return false;
					//아니면 현재 노드를 왼쪽으로 옮긴 후 계속
					else {
						now = now->left;
						continue;
					}
					//찾은 경우 종료
				case 0:return true;
					//추가할 데이터가 뒤에 있을 때
				case 1:
					//현재 노드의 오른쪽이 비어있다면 종료
					if (now->right == NULL)
						return false;
					//아니면 현재 노드를 오른쪽으로 옮긴 후 계속
					else {
						now = now->right;
						continue;
					}
				}
			}
		}
	}
	//인덱스n의 데이터를 반환
	string find(int n) {
		//root가 null이면 공백문자열 반환
		if (root == NULL)
			return "";
		stack<node*> callStack;
		node* now = root;
		int count = n;
		while (true) {
			//트리의 왼쪽 끝으로 이동, 지나친 노드는 콜스택에 저장
			while (now->left != NULL) {
				callStack.push(now);
				now = now->left;
			}			
			while (true) {
				//n번째 노드를 찾았으면 값 반환
				if (count == 0)
					return now->value;
				else
					count--;
				//오른쪽 노드가 있으면 오른쪽으로 이동 후 처음부터 반복
				if (now->right != NULL) {
					now = now->right;
					break;
				}
				//n번째 노드가 없으면 공백문자열 반환
				if (callStack.getLength() == 0)
					return "";
				//오른쪽 노드가 없으면 콜스택에 저장된 노드로 이동
				else
				now = callStack.pop();
			}
		}
	}
	//메모리 해제 소멸자
	~BST() {
		//root가 null이면 종료
		if (root == NULL)
			return;
		stack<node*> callStack;
		node* now = root;
		node* tmp;		
		while (true) {
			//트리의 왼쪽 끝으로 이동, 지나친 노드는 콜스택에 저장
			while (now->left != NULL) {
				callStack.push(now);
				now = now->left;
			}
			while (true) {
				//오른쪽 노드가 있으면 현재 노드를 반환하고 오른쪽으로 이동 후 처음부터 반복
				if (now->right != NULL) {
					tmp = now;
					now = now->right;
					delete tmp;
					break;
				}
				//오른쪽 노드가 없으면 현재 노드를 반환하고 콜스택에 저장된 노드로 이동
				delete now;
				//더이상 노드가 없으면 종료
				if (callStack.getLength() == 0)
					return;
				else
					now = callStack.pop();
			}
		}
	}
};
int main() {
	BST bst;
	bst.add("c");
	bst.add("a");
	bst.add("b");
	bst.add("d");
	bst.add("e");
	std::cout << bst.find(4) << "\n";
	return 0;
}